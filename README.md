[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/alexmonnet/gitpod-remote-vscode)

# Gitpod Remote VSCode

A project where I test the feasibility of connecting a local VSCode instance to a gitpod container.